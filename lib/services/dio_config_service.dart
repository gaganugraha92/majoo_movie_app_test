import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance;
final String baseUrl = 'https://imdb8.p.rapidapi.com';

createInstance() async {
  var options = BaseOptions(
      baseUrl: baseUrl + "/auto-complete?q=game%20of%20thr",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "a124297260mshef85481b06d347ap153f8ajsn03e5b386e0f4",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  dioInstance.options.baseUrl = baseUrl + "/auto-complete?q=game%20of%20thr";
  return dioInstance;
}
