import 'package:majootestcase/database/database_helper.dart';

import '../models/user.dart';

class UserCtr {
  DatabaseHelper con = new DatabaseHelper();

//insertion
  Future<int> saveUser(User user) async {
    var dbClient = await con.db;
    int res = await dbClient.insert("User", user.toJson());
    return res;
  }

  //deletion
  Future<int> deleteUser(User user) async {
    var dbClient = await con.db;
    int res = await dbClient.delete("User");
    return res;
  }

  Future<User> getLogin(String email, String password) async {
    var dbClient = await con.db;
    var res = await dbClient.rawQuery(
        "SELECT * FROM User WHERE email = '$email' and password = '$password'");

    if (res.length > 0) {
      return new User.fromJson(res.first);
    }
    return null;
  }

  Future<bool> getUserIsExist(String email) async {
    var dbClient = await con.db;
    var res =
        await dbClient.rawQuery("SELECT * FROM User WHERE email = '$email'");

    if (res.length > 0) {
      return true;
    }
    return false;
  }

  Future<List<User>> getAllUser() async {
    var dbClient = await con.db;
    var res = await dbClient.query("User");

    List<User> list =
        res.isNotEmpty ? res.map((c) => User.fromJson(c)).toList() : null;
    return list;
  }
}
