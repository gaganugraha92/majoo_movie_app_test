import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/database/user_ctr.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitialState());

  UserCtr userCtr = UserCtr();

  Future<void> registerUser(User user) async {
    int result;
    emit(RegisterBlocInitialState());
    bool checkUser = await userCtr.getUserIsExist(user.email);
    emit(RegisterBlocLoadingState());

    if (!checkUser) {
      result = await userCtr.saveUser(user);
      if (result > 0) {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        await sharedPreferences.setBool("is_logged_in", true);
        emit(RegisterBlocSuccesState());
      } else {
        emit(RegisterBlocErrorState(
            'ada kesalahan saat menambahkan data, silahkan untuk coba lagi'));
      }
    } else {
      emit(RegisterBlocErrorState('email sudah digunakan'));
    }
  }
}
