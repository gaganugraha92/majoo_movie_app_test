import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

import '../../common/widget/text_form_field.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final _confirmPasswordController = TextController();
  final _usernameController = TextController();
  bool _isLoading = false;

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isObscurePassword = true;
  RegisterBlocCubit _registerlocCubit = RegisterBlocCubit();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Registrasi'),
        ),
        body: BlocProvider(
          create: (context) => _registerlocCubit,
          child: BlocListener<RegisterBlocCubit, RegisterBlocState>(
            listener: (context, state) {
              if (state is RegisterBlocSuccesState) {
                setState(() {
                  _isLoading = false;
                });

                Fluttertoast.showToast(
                    msg: "Register Berhasil",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeBlocScreen(),
                  ),
                );
              } else if (state is RegisterBlocErrorState) {
                setState(() {
                  _isLoading = false;
                });
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(state.error.toString()),
                ));
              } else if (state is RegisterBlocLoadingState) {
                setState(() {
                  _isLoading = true;
                });
              }
            },
            child: _buildLoaded(),
          ),
        ));
  }

  Widget _buildLoaded() {
    return SingleChildScrollView(
      child: Padding(
          padding: EdgeInsets.only(top: 10, left: 25, bottom: 25, right: 25),
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Silahkan untuk melakukan registrasi terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  _form(),
                  SizedBox(
                    height: 50,
                  ),
                  CustomButton(
                    text: 'Register',
                    onPressed: !_isLoading ? handleRegister : null,
                    height: 100,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
              _isLoading
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Container(),
            ],
          )),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'example',
            label: 'Username',
            validator: (val) {
              if (val.trim().length < 4) {
                return 'Username tidak boleh kurang dari 4 karakter';
              } else {
                return null;
              }
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) {
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
              } else {
                return null;
              }
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              if (val != null && val.length < 6) {
                return 'Password harus memiliki panjang minimal 6 karakter';
              } else {
                return null;
              }
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Konfirmasi Password',
            hint: 'konfirmasi password',
            controller: _confirmPasswordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              if (val != null) {
                if (val != null && val.length < 6) {
                  return 'Password harus memiliki panjang minimal 6 karakter';
                } else if (val != _passwordController.textController.text) {
                  return 'Konfirmasi password tidak sama';
                }
                return null;
              }

              return null;
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _username = _usernameController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      User user = User(
        email: _email,
        password: _password,
        userName: _username,
      );

      _registerlocCubit.registerUser(user);
    }
  }
}
