import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class DetailMoviePage extends StatelessWidget {
  final Data data;

  const DetailMoviePage({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Movie'),
      ),
      body: ListView(
        children: [
          Image.network(
            data.i.imageUrl,
            height: 300,
            fit: BoxFit.fitWidth,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text('title : ' + data.l, textDirection: TextDirection.ltr),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            child: Text('year : ' + data.year.toString(),
                textDirection: TextDirection.ltr),
          ),
          data.series != null
              ? Container(
                  height: 200,
                  margin: EdgeInsets.only(top: 20),
                  child: ListView.builder(
                    itemCount: data.series.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return movieItemWidget(data.series[index], context);
                    },
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Widget movieItemWidget(Series data, BuildContext context) {
    return Container(
      width: 180,
      child: Card(
        child: Column(
          children: [
            Image.network(
              data.i.imageUrl,
              fit: BoxFit.fill,
              height: 100,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.l, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }
}
